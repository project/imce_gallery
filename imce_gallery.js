function imce_gallery(uid) {
	the_element = parent.frames['imce-frame'].document.getElementById("bodytable").getElementsByTagName('td');
	
	for(a=0;a<the_element.length;a++) {
		for(b=0;b<the_element[a].attributes.length;b++) {
			if(the_element[a].attributes[b].nodeValue == 'filename') 
				filename = a;
			if(the_element[a].attributes[b].nodeValue == 'operations') {
				the_element[a].innerHTML = "<a href='#' onClick='parent.add_img(\""+the_element[filename].innerHTML+"\", "+uid+");void(0);'>add to gallery</a>&nbsp;&nbsp;"+the_element[a].innerHTML;
				break;
			}
		}
	}
}

function add_img(file, uid) {
	the_element = document.getElementById("imce-frame").style;
	the_element.display="none";
	
	the_div = document.getElementById("gallery_choice");
	
	$(document).ready( function() {
		$("#g_file_name").html(file);
   		$("#edit-filename").val(file);
 	});
	
	the_div.style.display="inline";
}

function get_orig(filename, fid) {
	fid = fid+1;
	prev = fid-1;
	next = fid+1;
	$(document).ready(function() {
		var div_exist = $("#overlay").parent().is("body");
		//alert(div_exist);
		if(div_exist) {
			$("#overlay").empty();
			$("#lightbox").empty();
		}
		else {
			$("body").append("<div id='overlay'></div>");
			$("body").append("<div id='lightbox'></div>");
		}

		var count_pix = $("img[@name='gallery_images']").length;

		$("#overlay").height(document.documentElement.clientHeight+"px");
		$("#overlay").width(document.documentElement.clientWidth+"px");
		$("#overlay").append("<div id='close_btn' align='left'></div>");
		$("#close_btn").append("<a href='#' id='close_link' onClick='close_div();'></a>");
		$("#close_link").append("<img src='/modules/imce_gallery/closelabel.gif' />");
		
		$("#lightbox").append("<div id='original_image' align='center'></div>");
		$("#original_image").append("<div id='image_info' align='center'></div>");
		$("#image_info").append("<a href='#' id='prev_link'></a>");
		$("#prev_link").click(function() { get_img(prev); } );
		$("#prev_link").append("<img src='/modules/imce_gallery/prev.gif' />");
		$("#prev_link").after("<span id='image_num'></span>");
		$("#image_num").append(fid+" of "+count_pix);
		$("#image_num").after("<a href='#' id='next_link'></a>");
		$("#next_link").click(function() { get_img(next); } );
		$("#next_link").append("<img src='/modules/imce_gallery/next.gif' />");
		
		$("#image_info").after("<img id='orig_img' src='"+filename+"' />");
		
		todo_with_buttons(fid, count_pix);		
		
		$("#overlay").css("display", "inline");
		$("#lightbox").fadeIn("normal");
   });
}
function change_img(filename, fid) {
	fid = fid+1;
	prev = fid-1;
	next = fid+1;
	
	var count_pix = $("img[@name='gallery_images']").length;
	
	todo_with_buttons(fid, count_pix);
	
	$("#orig_img").attr("src",filename);
	$("#image_num").empty();
	$("#image_num").append(fid+" of "+count_pix);
	$("#orig_img").fadeIn("normal");
}

function todo_with_buttons(fid, count_pix) {
	$("#prev_link").css("visibility", "visible");
	$("#next_link").css("visibility", "visible");
	
	if(fid == 1)
		$("#prev_link").css("visibility","hidden");
			
	if(fid == count_pix)
		$("#next_link").css("visibility","hidden");	
}

function close_div() {
	$("#overlay").css("display", "none");
	$("#lightbox").fadeOut("normal");
}

function get_img(fid) {
	fid=fid-1;
	
	the_img = $("#img_"+fid).get();
	filename = the_img[0].src;
	host = "http://"+window.location.hostname;
	filename = filename.replace(host, "");
		
	change_img(filename, fid);
}